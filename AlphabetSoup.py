# Program Description: AlphabetSoup.py takes user entry for a .txt file name.  It uses this file as
# a basis to run a word search algorithm. The format for the .txt file is:'#'x'#'\n (representing
# the grid size) and the corresponding char grid on the following lines with each char separated by
# a space. Following the grid, the search words are listed, each on a new line. Results are case
# sensitive, and will list the beginning and ending coordinates for the discovered word on the x:y
# grid, separated by a space. Valid word orientations are horizontal, vertical, diagonal, forwards,
# and backwards.  Package manager used for Python Implementation: PIP
# Author: John Schmidt
# Date: 2/24/2023

fileName = input("Please enter .txt file name (e.g., example.txt): ")

with open(fileName, "r") as file:
    # Select rows and columns of grid (#x#)
    rows = int(file.read(1))
    file.seek(2)
    cols = int(file.read(1))
    file.read(1)

    gridArray = [[0 for i in range(cols)] for j in range(rows)]

    # populate gridArray with grid of characters
    for r in range(rows):
        for c in range(cols):
            gridArray[r][c] = file.read(1)
            file.read(1)

    searchTerms = [line.rstrip() for line in file]

    # check each word against the grid
    for wordPos in range(len(searchTerms)):
        wordLength = len(searchTerms[wordPos])

        # check each letter in gridArray
        for i in range(rows):
            for j in range(cols):
                # check against first letter in each search word
                if searchTerms[wordPos][0] == gridArray[i][j]:
                    startPos = [i, j]

                    # search 8 surrounding directions for sufficient length for word to exist
                    # check horizontal forward
                    if (j + wordLength) <= cols:
                        tempColPos = j + 1
                        letterPos = 1

                        # check each letter of the search word against the letter in the grid
                        while letterPos < wordLength:
                            curLetter = searchTerms[wordPos][letterPos]

                            if curLetter == " ":  # skip spaces
                                letterPos += 1
                                continue
                            elif curLetter == gridArray[i][tempColPos]:  # letter match
                                letterPos += 1

                                if letterPos == wordLength:  # word match
                                    print(searchTerms[wordPos], end=" ")
                                    print(str(i) + ":" + str(j), end=" ")
                                    print(str(i) + ":" + str(tempColPos))

                                tempColPos += 1

                            else:
                                break

                    # check vertical up
                    if (i - (wordLength - 1)) >= 0:
                        tempRowPos = i - 1
                        letterPos = 1

                        # check each letter of the search word against the letter in the grid
                        while letterPos < wordLength:
                            curLetter = searchTerms[wordPos][letterPos]

                            if curLetter == " ":  # skip spaces
                                letterPos += 1
                                continue
                            elif curLetter == gridArray[tempRowPos][j]:  # letter match
                                letterPos += 1

                                if letterPos == wordLength:  # word match
                                    print(searchTerms[wordPos], end=" ")
                                    print(str(i) + ":" + str(j), end=" ")
                                    print(str(tempRowPos) + ":" + str(j))

                                tempRowPos -= 1

                            else:
                                break

                    # check diagonal NE
                    if ((j + wordLength) <= cols) and ((i - (wordLength - 1)) >= 0):
                        tempRowPos = i - 1
                        tempColPos = j + 1
                        letterPos = 1

                        # check each letter of the search word against the letter in the grid
                        while letterPos < wordLength:
                            curLetter = searchTerms[wordPos][letterPos]

                            if curLetter == " ":  # skip spaces
                                letterPos += 1
                                continue
                            elif (
                                curLetter == gridArray[tempRowPos][tempColPos]
                            ):  # letter match
                                letterPos += 1

                                if letterPos == wordLength:  # word match
                                    print(searchTerms[wordPos], end=" ")
                                    print(str(i) + ":" + str(j), end=" ")
                                    print(str(tempRowPos) + ":" + str(tempColPos))

                                tempRowPos -= 1
                                tempColPos += 1

                            else:
                                break

                    # check horizontal reverse
                    if (j - (wordLength - 1)) >= 0:
                        tempColPos = j - 1
                        letterPos = 1

                        # check each letter of the search word against the letter in the grid
                        while letterPos < wordLength:
                            curLetter = searchTerms[wordPos][letterPos]

                            if curLetter == " ":  # skip spaces
                                letterPos += 1
                                continue
                            elif curLetter == gridArray[i][tempColPos]:  # letter match
                                letterPos += 1

                                if letterPos == wordLength:  # word match
                                    print(searchTerms[wordPos], end=" ")
                                    print(str(i) + ":" + str(j), end=" ")
                                    print(str(i) + ":" + str(tempColPos))

                                tempColPos -= 1
                            else:
                                break

                    # check diagonal NW
                    if ((j - (wordLength - 1)) >= 0) and ((i - (wordLength - 1)) >= 0):
                        tempRowPos = i - 1
                        tempColPos = j - 1
                        letterPos = 1

                        # check each letter of the search word against the letter in the grid
                        while letterPos < wordLength:
                            curLetter = searchTerms[wordPos][letterPos]

                            if curLetter == " ":  # skip spaces
                                letterPos += 1
                                continue
                            elif (
                                curLetter == gridArray[tempRowPos][tempColPos]
                            ):  # letter match
                                letterPos += 1

                                if letterPos == wordLength:  # word match
                                    print(searchTerms[wordPos], end=" ")
                                    print(str(i) + ":" + str(j), end=" ")
                                    print(str(tempRowPos) + ":" + str(tempColPos))

                                tempRowPos -= 1
                                tempColPos -= 1

                            else:
                                break

                    # check vertical down
                    if (i + wordLength) <= rows:
                        tempRowPos = i + 1
                        letterPos = 1

                        # check each letter of the search word against the letter in the grid
                        while letterPos < wordLength:
                            curLetter = searchTerms[wordPos][letterPos]

                            if curLetter == " ":  # skip spaces
                                letterPos += 1
                                continue
                            elif curLetter == gridArray[tempRowPos][j]:  # letter match
                                letterPos += 1

                                if letterPos == wordLength:  # word match
                                    print(searchTerms[wordPos], end=" ")
                                    print(str(i) + ":" + str(j), end=" ")
                                    print(str(tempRowPos) + ":" + str(j))

                                tempRowPos += 1

                            else:
                                break

                    # check diagonal SE
                    if ((j + wordLength) <= cols) and ((i + wordLength) <= rows):
                        tempRowPos = i + 1
                        tempColPos = j + 1
                        letterPos = 1

                        # check each letter of the search word against the letter in the grid
                        while letterPos < wordLength:
                            curLetter = searchTerms[wordPos][letterPos]

                            if curLetter == " ":  # skip spaces
                                letterPos += 1
                                continue
                            elif (
                                curLetter == gridArray[tempRowPos][tempColPos]
                            ):  # letter match
                                letterPos += 1

                                if letterPos == wordLength:  # word match
                                    print(searchTerms[wordPos], end=" ")
                                    print(str(i) + ":" + str(j), end=" ")
                                    print(str(tempRowPos) + ":" + str(tempColPos))

                                tempRowPos += 1
                                tempColPos += 1

                            else:
                                break

                    # check diagonal SW
                    if ((j - (wordLength - 1)) >= 0) and ((i + wordLength) <= rows):
                        tempRowPos = i + 1
                        tempColPos = j - 1
                        letterPos = 1

                        # check each letter of the search word against the letter in the grid
                        while letterPos < wordLength:
                            curLetter = searchTerms[wordPos][letterPos]

                            if curLetter == " ":  # skip spaces
                                letterPos += 1
                                continue
                            elif (
                                curLetter == gridArray[tempRowPos][tempColPos]
                            ):  # letter match
                                letterPos += 1

                                if letterPos == wordLength:  # word match
                                    print(searchTerms[wordPos], end=" ")
                                    print(str(i) + ":" + str(j), end=" ")
                                    print(str(tempRowPos) + ":" + str(tempColPos))

                                tempRowPos += 1
                                tempColPos -= 1

                            else:
                                break
